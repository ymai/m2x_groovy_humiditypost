package com.att.mss.m2m.M2X
/*
 * M2XDeviceStreamTest.groovy - Groovy script used to push data to M2X
 *
 * See README file on how to run this script
 */
//@Grab(group='com.att.m2x', module='m2x-java-client', version='2.0.1')
//@Grab(group='com.att.mss.m2m.device', module='libDHT', version='1.0.1-SNAPSHOT')
//@Grab(group='org.slf4j', module='slf4j-api', version='1.7.10')
//@Grab(group='ch.qos.logback', module='logback-classic', version='1.1.2')
import com.att.m2x.client.M2XClient
import com.att.m2x.client.M2XDevice
import com.att.m2x.client.M2XResponse
import com.att.m2x.client.M2XStream
import org.json.JSONArray
import org.json.JSONObject
import java.text.DateFormat
import java.text.SimpleDateFormat
import com.att.mss.m2m.device.*
import groovy.json.JsonBuilder
import java.io.Console
import groovy.transform.Field
//import groovy.util.logging.Slf4j  //only needed if using class annotation
import org.slf4j.*


def _stream;
def _dht;
//@Field Boolean _continue=true;
_continue=true;


//Logger log = Logger.getLogger( ClassName.class.getName() );
@Field Logger logger = LoggerFactory.getLogger("M2XDeviceStreamTest");

synchronized void trace(String log, Object... b) {
	logger.info (String.format(log, b));
}

def read() {
	_dht.read();
	return _dht.getHumidity();
}

def format_timestamp(date) {
	TimeZone tz = TimeZone.getTimeZone("UTC")
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'")
		df.setTimeZone(tz)
		return df.format(date)
}

def synchronized init(key, deviceID, streamName) {
	client = new M2XClient(key);
	device = client.device(deviceID);
	_stream = device.stream(streamName);

	_dht = DHTFactory.create(1);
}


// SendInterval in seconds
def readLoop(int sendInterval) {
	int threadWait = 500; //ms
	int sendWait = sendInterval * 1000 / threadWait;
	int i = sendWait;
	while (_continue) {
		if (i >= sendWait)
		{
			i = 0;
			def h = read();
			send(h);
		}
		else
		{
			i++;
		}
		sleep threadWait;
	}
}

def synchronized send(humidity) {
	// Push data to the stream, please refer to the Java library documentation
	// for details
	// use jsonbuilder
	def jb = new JsonBuilder();
	jb value: humidity, timestamp: format_timestamp(new Date())

//jsonValue = M2XClient.jsonSerialize(new HashMap<String, Object>(){{
//put("value", pushValue);
//put("timestamp", format_timestamp(new Date()));
//}}
//println jsonValue.toString();

	jsonValue = jb.toString()
	trace("json is "+jsonValue)
	def putResponse = _stream.updateValue(jsonValue)
	trace ("result is %d", putResponse.status);
	return putResponse.status;
}

def main() {
	if (args.size() < 4)
	{
		trace("Usage: groovy M2XDeviceStreamTest.groovy <key> <deviceId> <stream name> <transmit interval>");
		System.exit(1);
	}
	trace("key=%s\ndevice=%s\nstream=%s\ntransmit interval=%s", args[0], args[1], args[2], args[3]);
	init(args[0], args[1], args[2]);
	sendThread = Thread.start {
		readLoop(args[3].toInteger());
	}

	Console console = System.console();
	if (console != null)
	{
		consoleinput = console.readLine("Press 2 to stop\n");

		while (sendThread.isAlive()) {
			if (consoleinput.trim().equalsIgnoreCase("2")) {
				_continue = false;
			} else {
				trace("Invalid input");
				consoleinput = consoleconsole.readLine("Press 2 to stop\n");
			}
		}
	}
}

main();
